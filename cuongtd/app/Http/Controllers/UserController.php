<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Http\Requests\CreateUseRequests;
use App\Models\User;

class UserController extends Controller
{
    protected $users;

    public function __construct(User $users)
    {
        $this->users = $users;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   $data = $request->all();
        $users = $this->users->getUser($data);
        return view('user.index', ['users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(createUseRequests $request)
    {
        $this->users->createUser($request->all());
        flash('Thêm mới người dùng thành công')->success();
        return redirect()->route('register.index');
    }

    public function create()
    {
        return view('user.add');
    }
}
